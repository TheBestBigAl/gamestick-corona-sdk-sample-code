local widget = require "widget"
local GSIcons = require "GSIcons"

--The plugin will ONLY work on a GameStick, so you will not be able to make any calls unless the app is running on a GameStick
gamestick = require "plugin.gamestick"

--Cache screen width and height as global values.
_W, _H = display.contentWidth, display.contentHeight

 --Object forward declarations.
local reticule, icons
local buttons = {}

--these timers will be used to allow a stick to be held in one direction and perform a repetitive action
--at set intervals (in this case scrolling through menu options every 0.5 seconds)
local moveUpAxisTimer, moveDownAxisTimer

--This is used to create a 'deadzone' on the joystick, so that small movements can be ignored
local minAxis = 0.25

--Used to record whether a key is already pressed down or not.
--This is not mandatory, but useful for this sample.
local keysPressed = {
    buttonA = false, 
    buttonB = false, 
    buttonX = false, 
    buttonY = false,
    up      = false,
    down    = false, 
    left    = false, 
    right   = false,
    back    = false,
    leftJoystickButton = false, --L3
    rightJoystickButton = false, --R3
    leftShoulderButton1 = false,
    rightShoulderButton1 = false,
}

--Table of joystick descriptors, just to make checking which stick has moved easier to read.
--Note that the dpad is also treated as axes, and not as keys.
local axisPressed = {
    [1] = "Joystick 1: Axis 1", --left joystick X axis
    [2] = "Joystick 1: Axis 2", --left joystick Y axis
    [3] = "Joystick 1: Axis 3", --right joystick X axis
    [4] = "Joystick 1: Axis 4", --right joystick Y axis
    [5] = "Joystick 1: Axis 5", --not used
    [6] = "Joystick 1: Axis 6", --not used
    [7] = "Joystick 1: Axis 7", --dpad X axis
    [8] = "Joystick 1: Axis 8", --dpad Y axis
}


--on screen text to display gamestickListener data
local feedbackText = display.newText("Listener text will appear here", _W * 0.6, 0, _W * 0.4, _H, native.systemFont, 20)
feedbackText:setReferencePoint(display.TopLeftReferencePoint)
feedbackText.x, feedbackTexty = _W * 0.6, 0




--Rerenders on-screen text, and resets position to prevent reference point errors
--due to size of text box changing.
local function updateText(str)
	feedbackText.text = str
	feedbackText:setReferencePoint(display.TopLeftReferencePoint)
	feedbackText.x, feedbackText.y =  _W * 0.6, 0
end


--The main listener function, this receives data back from the GameStick servers.
local function gamestickListener( event )

	updateText("gamestickListener called")
	--In the event of an error, an isError field will be present.
	--This could be due to missing or incorrect params when calling a gamestick function
	--or due to errors on the GameStick servers.
	if event.isError then
		native.showAlert( "Error: "..event.name, event.message, { "OK" } )
	else

		--All valid return fields are included in this sample.
		--You should not receive any fields other than these.
		if event.name == "GetAllAchievements" then
			local id  = event.id --a custom arg that you can send to more easily match up requests + responses
			local str = ""

			if event.data then

				local data = event.data -- a table of tables
		
				for i = 1, #data do
					local ItemID      = data[i].id --number
					local Name        = data[i].name --string
					local Description = data[i].description --string
					local Type        = data[i].type --string
					local FileName    = data[i].fileName --string
					local FileURL     = data[i].fileUrl --string
					local Completed   = data[i].completed --bool
					local XPValue     = data[i].xpValue	--number
					str = str.."\n".."itemId = "..ItemID..", Name = "..Name..", Description = "..Description..", Type = "..Type..", FileName = "..FileName..", FileURL = "..FileURL..", Completed = "..tostring(Completed)..", XPValue = "..XPValue

				end
			else
				--If achievements do not exist for this game yet
				str = "no data found"
			end
			updateText("name = "..event.name..", id = " ..event.id..", data = "..str)

		elseif event.name == "SetAchievementComplete" then
			local id = event.id --a custom arg that you can send to more easily match up requests + responses
			updateText("name = "..event.name..", event.id = "..event.id)

		elseif event.name == "LoadState" then		
			local id    = event.id --a custom arg that you can send to more easily match up requests + responses
			local input = event.input --string
			updateText("name = "..event.name..", input = "..input..", id = "..id)

		elseif event.name == "SaveState" then
			local id = event.id --a custom arg that you can send to more easily match up requests + responses
			updateText("name = "..event.name..", id = "..id)

		elseif event.name == "IAPGetItemsForPurchase" then
			local id  = event.id --a custom arg that you can send to more easily match up requests + responses
			local str = ""
			if event.data then
		
				local data = event.data -- a table of tables
				for i = 1, #data do
					local ItemID      = data[i].id --string
					local Name        = data[i].name --string
					local Description = data[i].description --string
					local Cost        = data[i].cost --string
					local Currency    = data[i].currency --string
					str = str.."\n".."itemId = "..ItemID..", Name = "..Name..", Description = "..Description..", Cost = "..Cost..", Currency = "..Currency	
				end
			end
			updateText("name = "..event.name..", data = "..str)

		elseif event.name == "IAPGetPurchasedItemURL" then
			local id  = event.id --a custom arg that you can send to more easily match up requests + responses
			local url = event.url --string
			updateText("name = "..event.name..", id = "..id..", url = "..url)

		elseif event.name == "IAPGetPurchasedItems" then
			local id  = event.id --a custom arg that you can send to more easily match up requests + responses
			local str = ""
			if event.data then
				local data = event.data -- a table of tables
				for i = 1, #data do
					local ItemID      = data[i].id --string
					local Name        = data[i].name --string
					local Description = data[i].description --string
					local Cost        = data[i].cost --string
					local Currency    = data[i].currency --string
					str = str.."\n".."itemId = "..ItemID..", Name = "..Name..", Description = "..Description..", Cost = "..Cost..", Currency = "..Currency						
				end
			else
				str = "no data found"
			end
			updateText("name = "..event.name..", data = "..str)

		elseif event.name == "IAPPurchaseItem" then
			local id       = event.id --a custom arg that you can send to more easily match up requests + responses
			local response = event.response --boolean -> was the purchase successful or not?
			updateText("name = "..event.name..", id = "..id..", response = "..tostring(response))

		elseif event.name == "LBGetTop50" then
			local id  = event.id --a custom arg that you can send to more easily match up requests + responses
			local str = ""
			if event.data then
				local data = event.data -- a table of tables
				for i = 1, #data do
					local AvatarID = data[i].avatarID --number
					local Name     = data[i].name --string
					local Score    = data[i].score --number
					local Position = data[i].position --number		
					str = str.."\n".."AvatarID = "..AvatarID..", Name = "..Name..", Score = "..Score..", Position = "..Position
	
				end
			end
			updateText("name = "..event.name..", data = "..str)


		elseif event.name == "LBSaveScore" then
			local id = event.id --a custom arg that you can send to more easily match up requests + responses
			updateText("name = "..event.name..", id = "..id)
		end
	end
end



--Rerender the reticule's position
local function positionReticule()
	reticule.x, reticule.y = buttons[reticule.index].x, buttons[reticule.index].y
end


--The buttons can be pressed using a mouse
local function handleButtonEvent(e)
	if e.phase == "ended" then
		reticule.index = e.target.index
		positionReticule()

		updateText("function called")
		e.target.releaseEvent(e.target.args[1], e.target.args[2])
	end
end


--Table of functions assigned to each button - all current gamestick sdk function calls are included here:
--func:The function to call
--name:The label printed on the button
--args:The arguements to pass. Argument 1 will always be a table of parameters, argument 2 is always your gamestickListener function
--All functions have an option parameter "requestID". This parameter allows you to more easily match up requests and returned data. 
--For example, if you make 2 purchase requests close together and want to confirm which one you are receiving a reply from,
--you can compare the id field in the returned data to the requestID in your request. 
--If this parameter is left blank, you will still receive an id of 0 in the return data.
local GSfunction = {
	{func = gamestick.LBGetTop50, 				name = "Leaderboard Get Top 50", 		args = {{requestID = 4}, gamestickListener}},
	{func = gamestick.LBSaveScore, 				name = "Leaderboard Save Score", 		args = {{score = 10, requestID = 7}, gamestickListener}},
	{func = gamestick.SaveState, 				name = "Save State", 					args = {{data = "dataByteTestString", requestID = 8}, gamestickListener}},
	{func = gamestick.LoadState, 				name = "Load State", 					args = {{requestID = 9}, gamestickListener}},
	{func = gamestick.GetAllAchievements, 		name = "Get All Achievements", 			args = {{requestID = 10}, gamestickListener}},
	{func = gamestick.SetAchievementComplete, 	name = "Set Achievement Complete", 		args = {{achievementID = "231", requestID = 11}, gamestickListener}},
	{func = gamestick.IAPGetItemsForPurchase, 	name = "Get Items For Purchase", 		args = {{requestID = 14}, gamestickListener}},
	{func = gamestick.IAPGetPurchasedItemURL, 	name = "Get Purchased Item URL", 		args = {{itemID = "18", requestID = 16}, gamestickListener}},
	{func = gamestick.IAPGetPurchasedItems, 	name = "Get Purchased Items", 			args = {{requestID = 13}, gamestickListener}},
	{func = gamestick.IAPPurchaseItem, 			name = "Purchase Item", 				args = {{itemID = "18", requestID = 15}, gamestickListener}},
}




--Create buttons for each function
--These can be touched using a mouse to replicate an on screen touch or you can controller 1 to highlight them
for i = 1, 10 do
	local button = widget.newButton
	{
	    left = 0,
	    top = 0,
	    width = 350,
	    height = (_H / 20),
	    fontSize = 20,
	    label = GSfunction[i].name,
	    onEvent = handleButtonEvent,
	}

	button.releaseEvent = GSfunction[i].func
	button.args = GSfunction[i].args
	button.index = i

	if i < 6 then
		button.x = _W * 0.15
		button.y = 50 + (_H / 14) * i
	else
		button.x = _W * 0.45
		button.y = 50 + (_H / 14) * (i-5)
	end

	buttons[i] = button
end


--Create reticule to highlight buttons.
reticule = display.newRoundedRect(0,0,  350 - 4, (_H / 20) - 4, 10)
reticule:setReferencePoint(display.CenterReferencePoint)
reticule.x, reticule.y = buttons[1].x, buttons[1].y
reticule:setFillColor(255, 255, 0, 96)
reticule.index = 1




--Create an on-screen GameStick controller.
GSIcons.createIcons()



--If stick is held in one direction, a timer.performWithDelay function will move the reticule once every 0.5 seconds.
--When the stick is released or changes direction this function cancels the timer.performWithDelay.
local function cancelMove(dir)
    if dir == "up" then
        if moveUpAxisTimer then
            timer.cancel(moveUpAxisTimer)
            moveUpAxisTimer = nil
        end
    else
        if moveDownAxisTimer then
            timer.cancel(moveDownAxisTimer)
            moveDownAxisTimer = nil
        end
    end
end


--Move the reticule up.
local function moveUp()
	--only move if not already on a top button
    if reticule.index ~= 1 and reticule.index ~= 6 then
    	reticule.index = reticule.index - 1
    	positionReticule()  
    end
end

--Move the reticule down.
local function moveDown()
	--only move if not already on a bottom button
    if reticule.index ~= 5 and reticule.index ~= 10 then
    	reticule.index = reticule.index + 1
    	positionReticule()
    end  
end




--This function handles input from joysticks, dpads, shoulder buttons and mouse.
function onAxisEvent( event )

		print(event.axis.descriptor, event.normalizedValue)

		--if left stick or dpad moved up/down
        if event.axis.descriptor == axisPressed[2] or event.axis.descriptor == axisPressed[8] then

        	--if stick/dpad has moved up more than my custom min value (used to create a deadzone)
            if event.normalizedValue < -minAxis then  

            	--cancel any down movements
                cancelMove("down")

                --move up and set a timer to continuously move up as long as up is held
                if not moveUpAxisTimer then
                    moveUp()
                    moveUpAxisTimer = timer.performWithDelay(500, moveUp, -1)
                end

            --as above, but for down
            elseif event.normalizedValue > minAxis then
                cancelMove("up")                   
                if not moveDownAxisTimer then
                    moveDown()
                    moveDownAxisTimer = timer.performWithDelay(500, moveDown, -1)  
                end  
            else
            	--if stick is in deadzone, then cancel any up/down movements
                cancelMove("down")
                cancelMove("up")          
            end 


        --if left stick or dpad moved left/right
        elseif event.axis.descriptor == axisPressed[1] or event.axis.descriptor == axisPressed[7] then

            --if left
            if event.normalizedValue < -minAxis then 
            	--move reticule to buttons on left (if not already there) 
                if reticule.index >= 6 then
                    reticule.index = reticule.index - 5
                    positionReticule()
                end

            --if right
            elseif event.normalizedValue > minAxis then
            	--move reticule to buttons on right (if not already there) 
                if reticule.index <= 5 then
                    reticule.index = reticule.index + 5
                    positionReticule()
                end
            end   
        end


        --normally you would probably want to perform all actions for each axis in one place, but to make things clearer
        --the onscreen gamestick controller images are moved/scaled separately here

        if event.axis.descriptor == axisPressed[1] then
        	GSIcons.axes.leftStick.x = GSIcons.axes.leftStick.centerx + (event.normalizedValue * (GSIcons.axes.leftStick.width * 0.5))
        elseif event.axis.descriptor == axisPressed[2] then
        	GSIcons.axes.leftStick.y = GSIcons.axes.leftStick.centery + (event.normalizedValue * (GSIcons.axes.leftStick.height * 0.5))
        elseif event.axis.descriptor == axisPressed[3] then
        	GSIcons.axes.rightStick.x = GSIcons.axes.rightStick.centerx + (event.normalizedValue * (GSIcons.axes.rightStick.width * 0.5))
        elseif event.axis.descriptor == axisPressed[4] then
        	GSIcons.axes.rightStick.y = GSIcons.axes.rightStick.centery + (event.normalizedValue * (GSIcons.axes.rightStick.height * 0.5))
        elseif event.axis.descriptor == axisPressed[5] then
        
        elseif event.axis.descriptor == axisPressed[6] then
        	
        elseif event.axis.descriptor == axisPressed[7] then
        	if event.normalizedValue < -minAxis then 
				GSIcons.axes.dpadLeft.xScale = 2
				GSIcons.axes.dpadLeft.yScale = 2
			elseif event.normalizedValue > minAxis then
				GSIcons.axes.dpadRight.xScale = 2
				GSIcons.axes.dpadRight.yScale = 2
			else
				GSIcons.axes.dpadLeft.xScale = 1
				GSIcons.axes.dpadLeft.yScale = 1
				GSIcons.axes.dpadRight.xScale = 1
				GSIcons.axes.dpadRight.yScale = 1
			end
        elseif event.axis.descriptor == axisPressed[8] then
        	if event.normalizedValue < -minAxis then 
				GSIcons.axes.dpadUp.xScale = 2
				GSIcons.axes.dpadUp.yScale = 2
			elseif event.normalizedValue > minAxis then
				GSIcons.axes.dpadDown.xScale = 2
				GSIcons.axes.dpadDown.yScale = 2
			else
				GSIcons.axes.dpadUp.xScale = 1
				GSIcons.axes.dpadUp.yScale = 1
				GSIcons.axes.dpadDown.xScale = 1
				GSIcons.axes.dpadDown.yScale = 1
			end
        end

    return true
end


--Called when a key event (button press) has been received.
local function onKeyEvent( event )
   	
    if event.keyName == "back" then

    	--when back key is pressed, check to see if user wants to exit
    	local function onComplete(event)
            if(event.index == 1) then
                native.requestExit()
            end
        end
        local alert = native.showAlert( "Exit?", "Are you sure you want to exit this app?", { "Yes","No"},onComplete )      
    else

    	--event.phase == "down" means the key is pressed
	    if event.phase == "down" then 

	    	--check keyname
	    	if event.keyName == 'buttonA' then
	    		--if key is not already held down then proceed
	    		if keysPressed.buttonA ~= true then
	    			--record that key IS now held down, in case we want to prevent 
	    			--simultaneous presses of certain buttons (cannot 'block' and 'fire gun' for instance)
	    		    keysPressed.buttonA = true
	    		    --call the function that the reticule is currently highlighting
	    			GSfunction[reticule.index].func(GSfunction[reticule.index].args[1], GSfunction[reticule.index].args[2])
	    		end
	    	end


	    	--if there is an image for this button, then scale up to show it has been pressed
	    	if GSIcons.keys then
				if GSIcons.keys[event.keyName] then
					transition.to(GSIcons.keys[event.keyName], {time = 100, xScale = 2, yScale = 2})
				end
			end

	    --event.phase == "down" means the key is released
	    elseif event.phase == "up" then 
	    	if keysPressed then
	    	    if keysPressed[event.keyName] then
	    	        keysPressed[event.keyName] = false
	    	    end
	    	end

	    	--on release set button image scale back to 1
	    	if GSIcons.keys then
				if GSIcons.keys[event.keyName] then
					transition.to(GSIcons.keys[event.keyName], {time = 100, xScale = 1, yScale = 1})
				end
			end
	    end
	    return true
	end
end




-- Add the key event gamestickListener.
Runtime:addEventListener( "key", onKeyEvent );
Runtime:addEventListener( "axis", onAxisEvent )




