local GSIcons = {}
	
	GSIcons.keys = {} 
	GSIcons.axes = {}
	local group = display.newGroup()

	function GSIcons.createIcons()

		local dWidth, dHeight = 26, 36

		--controller image
		local controller = display.newImageRect( group, "images/gs_bg.png", 480, 230 )
		controller:setReferencePoint(display.CenterReferencePoint)
		controller.x, controller.y = _W * 0.3, _H * 0.8

		--axes images
		local leftStickBG = display.newImageRect( group, "images/gs_analog_bg.png", 64, 64 )
		leftStickBG:setReferencePoint(display.CenterReferencePoint)
		leftStickBG.x, leftStickBG.y = controller.x - 130, controller.y - 40

		local leftStick = display.newImageRect( group, "images/gs_analog.png", 64, 64 )
		leftStick:setReferencePoint(display.CenterReferencePoint)
		leftStick.x, leftStick.y = leftStickBG.x, leftStickBG.y
		leftStick.centerx, leftStick.centery = leftStick.x, leftStick.y
		GSIcons.axes.leftStick = leftStick
		GSIcons.keys.leftJoystickButton = leftStick

		local rightStickBG = display.newImageRect( group, "images/gs_analog_bg.png", 64, 64 )
		rightStickBG:setReferencePoint(display.CenterReferencePoint)
		rightStickBG.x, rightStickBG.y = controller.x + 80, controller.y + 30

		local rightStick = display.newImageRect( group, "images/gs_analog.png", 64, 64 )
		rightStick:setReferencePoint(display.CenterReferencePoint)
		rightStick.x, rightStick.y = rightStickBG.x, rightStickBG.y
		rightStick.centerx, rightStick.centery = rightStick.x, rightStick.y
		GSIcons.axes.rightStick = rightStick	
		GSIcons.keys.rightJoystickButton = rightStick

		local dpadUp = display.newImageRect( group, "images/gs_dpad_up.png", dWidth, dHeight )
		dpadUp:setReferencePoint(display.CenterReferencePoint)
		dpadUp.x, dpadUp.y = controller.x - 70, controller.y + 10
		dpadUp:setFillColor(200, 200, 200)
		GSIcons.axes.dpadUp = dpadUp

		local dpadDown = display.newImageRect( group, "images/gs_dpad_down.png", dWidth, dHeight )
		dpadDown:setReferencePoint(display.CenterReferencePoint)
		dpadDown.x, dpadDown.y = dpadUp.x, dpadUp.y + dpadUp.height + 4
		dpadDown:setFillColor(200, 200, 200)
		GSIcons.axes.dpadDown = dpadDown

		local dpadLeft = display.newImageRect( group, "images/gs_dpad_left.png", dHeight, dWidth )
		dpadLeft:setReferencePoint(display.CenterReferencePoint)
		dpadLeft.x, dpadLeft.y = dpadUp.x - dpadLeft.width * 0.5, dpadUp.y + 2 + dpadUp.height * 0.5
		dpadLeft:setFillColor(200, 200, 200)
		GSIcons.axes.dpadLeft = dpadLeft

		local dpadRight = display.newImageRect( group, "images/gs_dpad_right.png", dHeight, dWidth )
		dpadRight:setReferencePoint(display.CenterReferencePoint)
		dpadRight.x, dpadRight.y = dpadUp.x + dpadLeft.width * 0.5, dpadUp.y + 2 + dpadUp.height * 0.5
		dpadRight:setFillColor(200, 200, 200)
		GSIcons.axes.dpadRight = dpadRight

		local shoulderLeft = display.newImageRect( group, "images/gs_shoulder_l.png", 64, 32 )
		shoulderLeft:setReferencePoint(display.CenterReferencePoint)
		shoulderLeft.x, shoulderLeft.y = controller.x - controller.width * 0.5 + shoulderLeft.width, controller.y - 90
		shoulderLeft:setFillColor(200, 200, 200)
		GSIcons.keys.leftShoulderButton1 = shoulderLeft

		local shoulderRight = display.newImageRect( group, "images/gs_shoulder_r.png", 64, 32 )
		shoulderRight:setReferencePoint(display.CenterReferencePoint)
		shoulderRight.x, shoulderRight.y = controller.x + controller.width * 0.5 - shoulderRight.width, controller.y - 90
		shoulderRight:setFillColor(200, 200, 200)
		GSIcons.keys.rightShoulderButton1 = shoulderRight


		--key images
		local buttonA = display.newImageRect( group, "images/gs_button_a.png", 32, 32 )
		buttonA:setReferencePoint(display.CenterReferencePoint)
		buttonA.x, buttonA.y = controller.x + 132, controller.y - 10
		GSIcons.keys.buttonA = buttonA

		local buttonB = display.newImageRect( group, "images/gs_button_b.png", 32, 32 )
		buttonB:setReferencePoint(display.CenterReferencePoint)
		buttonB.x, buttonB.y = controller.x + 159, controller.y - 37
		GSIcons.keys.buttonB = buttonB

		local buttonX = display.newImageRect( group, "images/gs_button_x.png", 32, 32 )
		buttonX:setReferencePoint(display.CenterReferencePoint)
		buttonX.x, buttonX.y = controller.x + 105, controller.y - 37
		GSIcons.keys.buttonX = buttonX

		local buttonY = display.newImageRect( group, "images/gs_button_y.png", 32, 32 )
		buttonY:setReferencePoint(display.CenterReferencePoint)
		buttonY.x, buttonY.y = controller.x + 132, controller.y - 64
		GSIcons.keys.buttonY = buttonY


		local back = display.newRoundedRect( group, 0, 0, 28, 15, 3 )
		back:setReferencePoint(display.CenterReferencePoint)
		back.x, back.y = controller.x - 16, controller.y - 5
		back:setFillColor(200, 200, 200)
		GSIcons.keys.back = back

		local buttonStart = display.newRoundedRect( group, 0, 0, 28, 15, 3 )
		buttonStart:setReferencePoint(display.CenterReferencePoint)
		buttonStart.x, buttonStart.y = controller.x + 16, controller.y - 5
		buttonStart:setFillColor(200, 200, 200)
		GSIcons.keys.buttonStart = buttonStart

		
	end






return GSIcons